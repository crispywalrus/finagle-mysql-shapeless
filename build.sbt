
name := "finagle-mysql-shapeless"
organization := "llc.flyingwalrus"

val finagleVersion = "19.4.0"

libraryDependencies ++= Seq(
  "com.twitter" %% "finagle-mysql" % finagleVersion,
  "org.typelevel" %% "cats-core" % "1.6.0",
  "org.typelevel" %% "cats-effect" % "1.2.0",
  "io.catbird" %% "catbird-effect" % finagleVersion,
  "com.chuusai" %% "shapeless" % "2.3.3"
) ++ Seq(
  "org.scalatest" %% "scalatest" % "3.0.7"
).map(_ % Test)
