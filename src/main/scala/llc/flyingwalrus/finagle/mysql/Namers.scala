package llc.flyingwalrus
package finagle
package mysql

import cats._
import com.twitter.conversions.StringOps

/**
  * Namers rename values according to a few standarized patterns.
  */
trait Namer extends (String ⇒ String) {
  def andThen(f: String ⇒ String): Namer = new Namer {
    def apply(s: String) = f(Namer.this.apply(s))
  }
}

object Namer {

  implicit val namerCatsInstance = new Monoid[Namer] {
    def empty = Literal
    def combine(a: Namer, b: Namer) = b andThen a
  }

  implicit class NamerOps(a: Namer) {
    def :+:(b: Namer): Namer = Monoid.combine(a, b)
  }

  implicit val default: Namer = SnakeCase

  /**
    * does nothing at all, simply a passthrough for all name styles.
    */
  trait Literal extends Namer {
    def apply(scalaName: String): String = scalaName
  }
  object Literal extends Literal

  /**
    * covert all chars in a name to their lower case form.
    */
  trait LowerCase extends Namer {
    def apply(s: String): String = s.toLowerCase
  }
  object LowerCase extends LowerCase

  /**
    * covert all chars in a name to their UPPER case form.
    */
  trait UpperCase extends Namer {
    def apply(s: String): String = s.toUpperCase
  }
  object UpperCase extends UpperCase

  /**
    * snake_case namer. takes a string in camelCase and converts it to
    * snake_case.
    */
  trait SnakeCase extends Namer {
    def apply(s: String): String = StringOps.toSnakeCase(s)
  }

  object SnakeCase extends SnakeCase

  /**
    * camelCase namer. take a string in either kabob-case or
    * snake_case and remake it into camelCase.
    */
  trait CamelCase extends Namer {

    def apply(s: String) = StringOps.toCamelCase(s.replaceAll("-","_"))

  }

  object CamelCase extends CamelCase

  /**
    * kebob-case namer. takes a string in camelCase or snake_case and
    * converts it to kebob-case.
    */
  trait KebobCase extends Namer {

    def apply(s: String): String = SnakeCase(s).replaceAll("_", "-")

  }

  object KebobCase extends KebobCase

  /**
    * this wraps the name in mysql sql escape quotes so that reserved
    * words and other normally invalid sql names can be used.
    */
  trait Escape extends Namer {
    def apply(s: String): String = s"`$s`"
  }
  object Escape extends Escape

  /**
    * convert from pluralized names to singular names.
    */
  trait DePluralizer extends Namer {
    def apply(s: String): String =
      if (s.endsWith("s") || s.endsWith("S"))
        s.substring(0, s.length() - 2)
      else
        s
  }
  object DePluralizer extends DePluralizer

}
