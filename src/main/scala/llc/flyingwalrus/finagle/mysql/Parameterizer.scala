package llc.flyingwalrus
package finagle
package mysql

import cats._
import cats.implicits._
import com.twitter.finagle.mysql._
import com.twitter.util.{ Future, Try }
import io.catbird.util._
import shapeless._

/**
  * Finagle's mysql driver takes as sql parameters a vararg list of
  * `Parameter` which are intended to be created via the
  * `CanBeParameter` typeclass. This is meant to lift various types of
  * `Product` types (case classes an tuples) into that same vararg
  * list. Because finagle only supports positional parameters this
  * simple assumes you've done it right and listed the params in the
  * "correct" (i.e. query) order.
  */
trait Parameterizer[T] {
  def parameterize(value: T): List[Parameter]
}

trait ParameterizerImplicits {

  def instance[A](f: (A) ⇒ List[Parameter]): Parameterizer[A] =
    new Parameterizer[A] {
      def parameterize(value: A): List[Parameter] = f(value)
    }

  implicit val cnilParameterizer: Parameterizer[CNil] = instance { _ ⇒
    throw new UnsupportedOperationException("the world hates CNil!")
  }

  implicit def coproductParameterizer[H, T <: Coproduct](
    implicit
    hParameterizer: Parameterizer[H],
    tParameterizer: Lazy[Parameterizer[T]]): Parameterizer[H :+: T] = instance {
    case Inl(h) ⇒ hParameterizer.parameterize(h)
    case Inr(t) ⇒ tParameterizer.value.parameterize(t)
  }

  implicit val hnilParameterizer: Parameterizer[HNil] = instance { _ ⇒
    Nil
  }

  implicit def productParameterizer[H, T <: HList](
    implicit
    hParameterizer: Parameterizer[H],
    tParameterizer: Lazy[Parameterizer[T]]): Parameterizer[H :: T] = instance {
    case h :: t ⇒
      hParameterizer.parameterize(h) ++ tParameterizer.value.parameterize(t)
  }

  implicit def canBeParameterParameterizer[A](implicit ev: CanBeParameter[A]): Parameterizer[A] =
    instance(List(_))

}

object Parameterizer extends ParameterizerImplicits {

  def apply[A](implicit par: Parameterizer[A]): Parameterizer[A] = par

  def toParameter[A](a: A)(implicit p: Parameterizer[A]): List[Parameter] = p.parameterize(a)

  implicit def basicParameterizer[A, R](
    implicit
    gen: Generic.Aux[A, R],
    parameterizer: Parameterizer[R]): Parameterizer[A] =
    instance(a ⇒ parameterizer.parameterize(gen.to(a)))

}
