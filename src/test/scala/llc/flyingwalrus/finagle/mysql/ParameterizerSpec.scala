package llc.flyingwalrus
package finagle
package mysql

import cats._
import cats.implicits._
import com.twitter.finagle.mysql._
import org.scalactic.Equality
import org.scalatest._
import shapeless._

class PameterizerSpec extends WordSpec with Matchers {

  type MyTuple = Tuple2[Int, Boolean]

  implicit val eqParameter: Equality[Parameter] = new Equality[Parameter] {
    def areEqual(a: Parameter, b: Any): Boolean =
      b.isInstanceOf[Parameter] && b.asInstanceOf[Parameter].value == a.value
  }

  "Parameterizer" should {

    "summon a parameterizer for a tuple" in {
      val x = Parameterizer[MyTuple]
    }
    "summon a parameterizer for a case class" in {
      val x = Parameterizer[MyProduct]
      Parameterizer.toParameter(MyProduct(100, "one hundred"))
    }

    "Convert a tuple to a list of parameters" in {
      val para = implicitly[Parameterizer[MyTuple]]
      val t: MyTuple = (1, true)
      val params: List[Parameter] = para.parameterize(t)
      params should contain theSameElementsAs (List(Parameter.wrap(1), Parameter.wrap(true)))
    }

    "summon a parameterizer for everything" in {
      val a = Parameterizer[CoProd]
      val b = Parameterizer[MyValueClass]
      val c = Parameterizer[MyProduct]
      val d = Parameterizer[MyRecursiveProduct]
      val e = Parameterizer[ProductCoproduct]
      val f = Parameterizer[BeyondRecursiveProduct]
    }
  }
}
