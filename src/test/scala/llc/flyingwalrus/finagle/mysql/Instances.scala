package llc.flyingwalrus
package finagle
package mysql

case class MyValueClass(i: Int) extends AnyVal
case class MyProduct(i: Int, s: String)
case class MyRecursiveProduct(i: Int, s: String, mvc: MyValueClass)
case class BeyondRecursiveProduct(i: Int, mp: MyProduct)

sealed trait CoProd
case class A(i: Int) extends CoProd
case class B(a: Array[Byte]) extends CoProd

case class ProductCoproduct(i: Int, cp: CoProd, mvc: MyValueClass, mp: MyProduct)
