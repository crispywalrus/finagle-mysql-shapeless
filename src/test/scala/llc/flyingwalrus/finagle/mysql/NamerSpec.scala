package llc.flyingwalrus
package finagle
package mysql

import cats._
import cats.implicits._
import com.twitter.finagle.mysql._
import org.scalactic.Equality
import org.scalatest._
import shapeless._

class NamerSpec extends WordSpec with Matchers {

  import Namer._

  "Snake_Case" should {
    "convert A to _a" in {
      SnakeCase("snakeCase") should be("snake_case")
    }
    "deal with repleated caps" in {
      SnakeCase("myDEK") should be("my_dek")
    }
    "not choke on more than one case hop" in {
      SnakeCase("myDEKSuper") should be("my_dek_super")
    }
    "handle combined repeated caps and multiple case changes" in {
      SnakeCase("aBcDeFF") should be("a_bc_de_ff")
    }
  }

  "Kebob-Case" should {
    "convert A to _a" in {
      KebobCase("kebobCase") should be("kebob-case")
    }
    "deal with repleated caps" in {
      KebobCase("myDEK") should be("my-dek")
    }
    "not choke on more than one case hop" in {
      KebobCase("myDEKSuper") should be("my-dek-super")
    }
    "handle combined repeated caps and multiple case changes" in {
      KebobCase("aBcDeFF") should be("a-bc-de-ff")
    }
  }

  "Literal" should {
    "not change anything, ever" in {
      Literal("xxxx_S_z") should be("xxxx_S_z")
    }
  }

  "Escape" should {
    "wrap strings in ``" in {
      Escape("abc") should be("`abc`")
    }
  }

  "camelCase" should {
    "remove _ " in {
      CamelCase("my_dek_super") should be("myDekSuper")
      CamelCase("my_DEK_super") should be("myDEKSuper")
    }
    "remove -" in {
      CamelCase("my-dek-super") should be("myDekSuper")
      CamelCase("my-DEK-super") should be("myDEKSuper")
    }
  }

  "UPPERCASE" should {
    "only upcase" in {
      UpperCase("aBcD,eFFg") should be("ABCD,EFFG")
    }
  }

  "lowercase" should {
    "only downcase" in {
      LowerCase("aBcD,eFFg") should be("abcd,effg")
    }
  }

  "Namer" should {
    "combine instances" in {
      val lowerSnakeCase = SnakeCase :+: LowerCase
      lowerSnakeCase("myDEKSuper") should be("my_dek_super")
    }
  }
}
